﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text timeZone;

    public int lastSecond;
	void Update () {

        int second = (int) Time.timeSinceLevelLoad;
        
        if(lastSecond!=second){
            float min = second / 60;
            float sec = second % 60;
            if (min > 0)
                timeZone.text = "" + min + ":" + sec;
            else
                timeZone.text = ""+sec;
            lastSecond = second;
        }

	
	}
}
