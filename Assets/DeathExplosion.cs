﻿using UnityEngine;
using System.Collections;

public class DeathExplosion : MonoBehaviour {
    public string poolNameToLoad = "Letters";
    public PoolGenerator poolGenerator;


    public HasLife hasLife;


    public string letterswave ="Boum   Boum   Boum";
    public Transform fireReferencePoint;

    public float expolsionSpeedRatio=2f;

    void Start()
    {

        hasLife.onNoLifeAnymore += Death;
    }

    private void Death(HasLife obj)
    {


        if (poolGenerator == null)
        {
            poolGenerator = PoolGenerator.GetPool(poolNameToLoad);
        }
        if (poolGenerator == null) return;

        float rotOfByChar = 360f / letterswave.Length;
        fireReferencePoint.Rotate(Vector3.right * Random.Range(0f, 360f));

        foreach (char nextChar in letterswave.ToCharArray() )
        {

            if (nextChar != ' ') { 
                GameObject nextLetter = poolGenerator.GetNextAvailable();
                if (nextLetter != null)
                {
                    DoubleText letter = nextLetter.GetComponent<DoubleText>() as DoubleText;
                    if (letter != null)
                    {
                        letter.SetText(nextChar);

                        nextLetter.transform.position = fireReferencePoint.position;
                        FallingLetter fallingLetter = nextLetter.GetComponent<FallingLetter>() as FallingLetter;
                        if (fallingLetter != null) { 
                            fallingLetter.direction = fireReferencePoint.forward;
                            fallingLetter.speed *= expolsionSpeedRatio;
                        }
                        nextLetter.SetActive(true);
                    
                    
                    }
                }
            }
            fireReferencePoint.Rotate(Vector3.right * rotOfByChar);
        }
        Destroy(this);
    }

    void OnDestroy()
    {

        hasLife.onNoLifeAnymore -= Death;
    }


}
