﻿using UnityEngine;
using System.Collections;

public class MoveFromTo : MonoBehaviour {


    public Transform from;
    public Transform to;

    public float pct = 0;
    public float pctPerSecond = 0.5f;
    public bool goUp;

    
    void Update () {

        if (pct > 1f) goUp = false;
        else if (pct < 0f) goUp = true;

        pct += (Time.deltaTime * pctPerSecond) * (goUp ? 1f : -1f);

      this.transform.position = Vector3.Lerp(from.position, to.position, Mathf.Clamp(pct,0f,1f));


	}
}
