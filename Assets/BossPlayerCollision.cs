﻿using UnityEngine;
using System.Collections;

public class BossPlayerCollision : MonoBehaviour {

    public float dommageByCollision = 1f;
    public void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Debug.Log("Collision");
            HasLife life = col.gameObject.GetComponent<HasLife>() as HasLife;
            if (life)
            {
                life.Life -= dommageByCollision;
                Debug.Log("Enemy life " + life.Life);
            }
            
        }


    }
}
