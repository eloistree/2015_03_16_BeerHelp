﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RestartTimer : MonoBehaviour {


    public float countDown = 9;
    public Text textDisplay;
	
	void Update () {
        countDown-=Time.deltaTime;
        if(countDown<0f)countDown=0f;
        textDisplay.text =""+((int) countDown);

        if (countDown <= 0f)
            Application.LoadLevel(Application.loadedLevel);
	}
}
