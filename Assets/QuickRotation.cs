﻿using UnityEngine;
using System.Collections;

public class QuickRotation : MonoBehaviour {


    public float speed=1f;
    public Vector3 axis = Vector3.one;
    void Update () {

        this.transform.Rotate(axis * (speed * Time.deltaTime));
	}
}
