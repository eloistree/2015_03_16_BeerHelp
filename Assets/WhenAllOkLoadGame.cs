﻿using UnityEngine;
using System.Collections;

public class WhenAllOkLoadGame : MonoBehaviour {


    public PoolGenerator[] pools;
    public float minTimeToLoad = 2f;
	
	// Update is called once per frame
	void Update () {
        if (IsOkToLoad())
        {
            Application.LoadLevel(1);
        }
	
	}

    public bool IsOkToLoad() {

        bool ok = true;
        if (Time.timeSinceLevelLoad < minTimeToLoad) return false;
        foreach(PoolGenerator pg in pools)
            if(!pg.hasBeenInitialized) return false;    
        return ok;

    }
}
