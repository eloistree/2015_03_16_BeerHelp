﻿using UnityEngine;
using System.Collections;

public class WordGenerator : MonoBehaviour {

    public string poolNameToLoad="Default";
    public PoolGenerator poolGenerator;
    public delegate void LetterCreated(char letter, Transform obj);
    public LetterCreated onLetterCreated;

    public delegate void Empty();
    public Empty onEmpty;
    public string incoming = "";

    public char sectorChar = ',';
    public char spaceChar = ' ';
    public char startTimerChar = '[';
    public char endTimerChar = ']';
    public float minBetweenSector = 1f;
    public float maxBetweenSector = 3f;

    public float timeBetweenLetter = 0.5f;
    public float timeBetweenWord = 0.5f;


    public float nextCountDown=0f;

    void Start() {

        
    
    }

	void Update () {
        if (poolGenerator == null)
        {
            poolGenerator = PoolGenerator.GetPool(poolNameToLoad);
        }
        if (poolGenerator == null) return;
        if (nextCountDown > 0)
            nextCountDown -= Time.deltaTime;
        else if (incoming.Length > 0)
        {
            char nextChar = GenerateNextLetter();
            //Debug.Log("> " + nextChar);
            if (nextChar != spaceChar && nextChar != sectorChar)
            {
                GameObject nextLetter = poolGenerator.GetNextAvailable();
                if (nextLetter != null)
                {
                    DoubleText letter = nextLetter.GetComponent<DoubleText>() as DoubleText;
                    if (letter != null)
                    {
                        letter.SetText(nextChar);
                        nextLetter.SetActive(true);
                        if (onLetterCreated != null)
                        {
                            onLetterCreated(nextChar, nextLetter.transform);
                        }
                    }
                }
            }
            if (nextChar == sectorChar)
                nextCountDown = Random.RandomRange(minBetweenSector, maxBetweenSector);
            else if (nextChar == spaceChar)
                nextCountDown = timeBetweenWord;
            else nextCountDown = timeBetweenLetter;
        }
        else {
            nextCountDown = 0;

            if (onEmpty != null)
                onEmpty();
        }
	}

    private char GenerateNextLetter()
    {
        char result = incoming[0];
        incoming = incoming.Remove(0, 1);
        return result;

    }
}
