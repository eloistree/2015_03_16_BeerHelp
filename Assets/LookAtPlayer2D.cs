﻿using UnityEngine;
using System.Collections;

public class LookAtPlayer2D : MonoBehaviour {

    public GameObject player;
	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");
	}
	
	void Update () {
        if (player == null) return;
        Vector3 playerPos = player.transform.position;
        playerPos.z = transform.position.z;
        transform.LookAt(playerPos);

	
	}
}
