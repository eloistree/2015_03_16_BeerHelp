﻿using UnityEngine;
using System.Collections;

public class RandomMove : MonoBehaviour {

    public Vector3 nextPosition;
    public float speed= 2f;
    public float depth = 5;

    void Start() {
        NewPosition();
    }
	void Update () 
    {

        this.transform.position = Vector3.MoveTowards(this.transform.position, nextPosition, speed*Time.deltaTime);
        if (Vector3.Distance(nextPosition, this.transform.position) == 0f)
            NewPosition();

	
	}

    private void NewPosition()
    {
        nextPosition = Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), depth));
    }
}
