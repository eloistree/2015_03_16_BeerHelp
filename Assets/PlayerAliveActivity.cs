﻿using UnityEngine;
using System.Collections;

public class PlayerAliveActivity : MonoBehaviour {


    public GameObject[] activeWhenAlive;
    public GameObject[] activeWhenDeath;
    public  bool _playerAlive;

    public HasLife playerLife;

    public bool IsPlayerAlive
    {
        get { return _playerAlive; }
        set { _playerAlive = value;
        SwitchState(_playerAlive);
        }
    }

    private void SwitchState(bool alive)
    {
        foreach(GameObject g in activeWhenAlive)
            g.SetActive(alive);
        foreach(GameObject g in activeWhenDeath)
            g.SetActive(!alive);

    }
    
	void Update () {


        if (IsPlayerAlive && playerLife == null)
        {
            IsPlayerAlive = false;
        }


        if ( playerLife == null) {
         GameObject playerObj =   GameObject.FindWithTag("Player") as GameObject;
         if (playerObj != null) { 
             playerLife = playerObj.GetComponent<HasLife>() as HasLife;
             if (playerLife != null)
                 IsPlayerAlive = true;
         }
        }

	
	}
}
