﻿using UnityEngine;
using System.Collections;

public class RainLetterPositionner : MonoBehaviour {

    public WordGenerator wordGenerator;
    public Transform directionRef;
    public float speed = 1f;

	void Start () {

        wordGenerator.onLetterCreated += PositionLetterWhenCreated ;
	}

    private void PositionLetterWhenCreated(char letter, Transform obj)
    {
      obj.position = directionRef.position;
      FallingLetter fallingLetter =   obj.GetComponent<FallingLetter>() as FallingLetter;
      if (fallingLetter != null)
      {
          fallingLetter.direction = directionRef.forward;
          fallingLetter.speed = speed;
      }
    }
	

    void OnDestroy() {

        wordGenerator.onLetterCreated -= PositionLetterWhenCreated;
    }
}
