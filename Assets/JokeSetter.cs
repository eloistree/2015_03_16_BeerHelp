﻿using UnityEngine;
using System.Collections;

public class JokeSetter : MonoBehaviour {

    public WordGenerator wordGenerator;
    public string[] jokes;

    void Start() {
        wordGenerator.onEmpty += RechargeOfJoke ;
    }
    void OnDestroy() {
        wordGenerator.onEmpty -= RechargeOfJoke;
    }
    private void RechargeOfJoke()
    {
        wordGenerator.incoming = jokes[Random.Range(0,jokes.Length)];
    }
}
