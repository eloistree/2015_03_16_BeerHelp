﻿using UnityEngine;
using System.Collections;

public class MobGenerator : MonoBehaviour {


    public GameObject mobPrefab;
    public float minTimeRange = 20f;
    public float maxTimeRange = 80f;
    public float countDown = 10f;


    void Update()
    {

        countDown -= Time.deltaTime;
        if (countDown < 0f)
        {
            GameObject.Instantiate(mobPrefab,transform.position, mobPrefab.transform.rotation);
            countDown = Random.Range(minTimeRange, maxTimeRange);
        }

    }
}
