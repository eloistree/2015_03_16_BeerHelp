﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DonationInfo : MonoBehaviour {

    public static string DonatorNameRaw = "";
    public static string DonatorJokeRaw = "";
    public static string DonatorTextRaw = "";

    public static string _donatorNameRaw = "";
    public static string _donatorJokeRaw = "";
    public static string _donatorTextRaw = "";


    public static List<string> DonatorName = new List<string>();
    public static List<string> DonatorJoke = new List<string>();
    public static List<string> DonatorText = new List<string>();



    public WWW_GetPost nameGetter;

    public float recoverInfoTime = 1.8f;

    void Start() {

        
        if (nameGetter != null)
        {
            nameGetter.LoadWithCoroutine(false, true);

        }
        Invoke("RecoverInfo", recoverInfoTime);
    }

    void RecoverInfo() {
        if (nameGetter != null)
            _donatorNameRaw += DonatorNameRaw += nameGetter.TextLoaded;
    
        
    }




    internal void AddNameRaw(string nameRaw)
    {
        _donatorNameRaw += DonatorNameRaw += nameRaw;
    }

    internal void AddJokeRaw(string nameRaw)
    {

        _donatorJokeRaw  += DonatorJokeRaw += nameRaw;
    }

    internal void AddTextRaw(string nameRaw)
    {

        _donatorTextRaw += DonatorTextRaw += nameRaw;
    }


    void OnDestroy() {

        Debug.Log(DonatorNameRaw);
        Debug.Break();
    }

}
