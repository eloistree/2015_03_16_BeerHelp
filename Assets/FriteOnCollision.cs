﻿using UnityEngine;
using System.Collections;

public class FriteOnCollision : MonoBehaviour {

    public float dommage = 1;
    public void OnTriggerEnter2D(Collider2D col)
    {

        if (col.CompareTag("Boss") || col.CompareTag("EnemyUnit"))
        {
            HasLife life = col.gameObject.GetComponent<HasLife>() as HasLife;
            if (life)
            {
                life.Life -= dommage;
                Debug.Log("Enemy life " + life.Life);
            }
//            col.gameObject.SendMessage("Hit", SendMessageOptions.DontRequireReceiver);
            this.gameObject.SetActive(false);
        }


    }
}
