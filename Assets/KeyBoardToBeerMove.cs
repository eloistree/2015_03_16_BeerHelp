﻿using UnityEngine;
using System.Collections;

public class KeyBoardToBeerMove : MonoBehaviour {


    public BeerManMove beerMove;
    // Update is called once per frame
	void Update () {

        beerMove.direction.x = Input.GetAxis("Horizontal");
        beerMove.direction.y = Input.GetAxis("Vertical");
	}
}
