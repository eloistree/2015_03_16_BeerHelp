﻿using UnityEngine;
using System.Collections;

public class MoveToward : MonoBehaviour {

    public float speed = 5f;
	void Update () {

        transform.position += transform.forward * speed * Time.deltaTime;
	}
}
