﻿using UnityEngine;
using System.Collections;

public class BeerManRotation : MonoBehaviour {

    public Rigidbody2D rigBody;
    public float forcePerScroll = 5f;
	void Update () {


        if (Input.mouseScrollDelta.y != 0f)
            rigBody.AddTorque(Input.mouseScrollDelta.y*forcePerScroll);
	
	}
}
