﻿using UnityEngine;
using System.Collections;

public class FireDetectionToHead : MonoBehaviour {


    public HeadDisplay head;
    public WordGenerator wordGenerator;
	// Use this for initialization
	void Start () {
        wordGenerator.onLetterCreated += Fire;
	}

    void OnDestroy() {

        wordGenerator.onLetterCreated -= Fire;

    }



    private void Fire(char letter, Transform obj)
    {

        head.SetFireFor(0.3f);
    }
}
