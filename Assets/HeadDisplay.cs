﻿using UnityEngine;
using System.Collections;

public class HeadDisplay : MonoBehaviour {


    public Transform mouth;
    public Vector3 initialPosition;
    public float distanceOpen=0.1f;

    public float fireDuring=0f;

    private float lastPourcent;
    public float pourcent;
    public float speedOpenPerSecond=1f;
    public float speedClosePerSecond=1f;
    
    void Start () {
        initialPosition = mouth.localPosition;
	}

    public void SetFireFor(float time) {
        fireDuring = time;
    }
	// Update is called once per frame
	void Update () {

        if (fireDuring > 0f) { 
            fireDuring -= Time.deltaTime;
            if (fireDuring < 0f) fireDuring = 0f;
        }
        

        if (fireDuring>0f && pourcent < 1f)
        {
            pourcent += speedOpenPerSecond * Time.deltaTime;
        }
        if (fireDuring<=0f && pourcent > 0f)
        {
            pourcent -= speedClosePerSecond * Time.deltaTime;
        }
            

        if (lastPourcent != pourcent) {
            mouth.localPosition = initialPosition + Vector3.down * (distanceOpen * pourcent);
        }
	
	}
}
