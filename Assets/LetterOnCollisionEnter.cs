﻿using UnityEngine;
using System.Collections;

public class LetterOnCollisionEnter : MonoBehaviour {

    public float dommage = 10;

    public void OnTriggerEnter2D(Collider2D col) {

        if (col.CompareTag("Player"))
        {
            HasLife life = col.gameObject.GetComponent<HasLife>() as HasLife;
            if (life)
            {
                life.Life -= dommage;
                Debug.Log("Player life " + life.Life);
            }
            this.gameObject.SetActive(false);
        }

    }
}
