﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DoubleText : MonoBehaviour {


    public Text frontText;
    public Text backText;
	

    public void SetText(char c) {

        frontText.text = "" + c;
        backText.text = "" + c;
    }
}
