﻿using UnityEngine;
using System.Collections;
using UnityEditor;

 [CustomEditor(typeof(WWW_GetPost))]
public class WWW_GetPost_Inspector : Editor {


    public override void OnInspectorGUI()
    {


        DrawDefaultInspector();
        WWW_GetPost getPost = (WWW_GetPost) target;
        if (GUILayout.Button("Load",GUILayout.MaxWidth(80), GUILayout.Height(20)))
        {
            getPost.LoadWithCoroutine(true,true);        
        }
        string text = getPost.TextLoaded;
        if (text != null && text.Length > 0) { 
            EditorGUILayout.LabelField("Text loaded");
            EditorGUILayout.TextArea(text, GUILayout.MinWidth(100), GUILayout.MinHeight(120));
        }
        string error = getPost.Error;
        if (error != null && error.Length > 0){
            EditorGUILayout.LabelField("Error detected");
            EditorGUILayout.TextArea(error, GUILayout.MinWidth(100), GUILayout.MinHeight(50));
        }
    }

}
