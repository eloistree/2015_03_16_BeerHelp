﻿using UnityEngine;
using System.Collections;

public class BeerManMove : MonoBehaviour {

    public Rigidbody2D rigEngine;
    public Transform tranDirection;
    public Vector2 direction;

    public float lateralSpeed = 3f;
    public float downSpeed = 2f;
    public float frontSpeed = 4f;
    public ForceMode2D forceMode = ForceMode2D.Force;

	void Update () {

        if (direction.x != 0f)
        {
            rigEngine.AddForce(Vector2.right * (direction.x * lateralSpeed * Time.deltaTime),forceMode);
        }

        if (direction.y > 0f)
        {
            rigEngine.AddForce(Vector2.up * (direction.y * frontSpeed * Time.deltaTime), forceMode);
        }
        if (direction.y < 0f)
        {
            rigEngine.AddForce(Vector2.up * (direction.y * downSpeed * Time.deltaTime), forceMode);
        }

	}
}
