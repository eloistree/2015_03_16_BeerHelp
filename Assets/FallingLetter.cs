﻿using UnityEngine;
using System.Collections;

public class FallingLetter : MonoBehaviour {

    public float speed = 1f;
    public Vector3 direction = new Vector3(0,-1,0);

    private bool _isActive;

    public bool IsActive
    {
        get { return _isActive; }
        set { 
            _isActive = value;
            ResetDirection();
            
        }
    }

    void OnEnable()
    {
        if (direction == Vector3.zero)
        ResetDirection();
    }

    private void ResetDirection()
    {

            direction = transform.forward;
    }
	void Update () {

        //if(IsActive != this.gameObject.activeSelf)
        //{ IsActive = this.gameObject.activeSelf; }

        this.transform.position +=direction*speed*Time.deltaTime;
	}
}
