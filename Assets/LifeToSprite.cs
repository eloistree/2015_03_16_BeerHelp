﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeToSprite : MonoBehaviour {

    public Image image;
    public HasLife life;
    public float displaySpeed=0.1f;

    public float nextPct;
    public float currentPct;
	
	void Update () {
        float pctNow = life.GetPourcentCompareToInital();
        if (pctNow != currentPct) {
            nextPct = pctNow;
        }

        if (nextPct != currentPct) {
            currentPct = Mathf.MoveTowards(currentPct, nextPct, displaySpeed*Time.deltaTime);
            image.fillAmount = currentPct;
        }

	}
}
