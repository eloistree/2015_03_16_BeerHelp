﻿using UnityEngine;
using System.Collections;

public class LostDepthWithTime : MonoBehaviour {


    public float depthLostSpeed = 1f;
    
    void Update()
    {
        this.transform.Translate(Vector3.forward*(Time.deltaTime * depthLostSpeed ), Space.World);
	}
}
