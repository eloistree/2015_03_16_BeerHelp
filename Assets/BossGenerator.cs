﻿using UnityEngine;
using System.Collections;

public class BossGenerator : MonoBehaviour {


    public GameObject[] bossPrefab;
    public float timeBetweenBoss = 60f;
    public float countDown = 30f;
	
	
	void Update () {

        countDown -= Time.deltaTime;
        if (countDown < 0f) {
               GameObject.Instantiate( bossPrefab[Random.Range(0, bossPrefab.Length)]);
               countDown = timeBetweenBoss;
        }
	
	}
}
