﻿using UnityEngine;
using System.Collections;

public class ComeBackToCenterWhenOut : MonoBehaviour {


    public float power = 20;
    public Rigidbody2D rigBody;
	
	void Update () {

        Vector3 viewPosition = Camera.main.WorldToViewportPoint(transform.position);
        if (viewPosition.x < 0.024f || viewPosition.x > 1f || viewPosition.y < 0f || viewPosition.y > 1f) {

            Vector3 newVelocity = rigBody.velocity;
            if (viewPosition.x <= 0.024f && newVelocity.x < 0f)
                newVelocity.x = 0f;

            if (viewPosition.y <= 0f && newVelocity.y < 0f)
                newVelocity.y = 0f;

            if (viewPosition.x >= 1f && newVelocity.x > 0f)
                newVelocity.x = 0f;

            if (viewPosition.y >= 1f && newVelocity.y > 0f)
                newVelocity.y = 0f;
               

                rigBody.velocity = newVelocity;
        }
        
	
	}
}
