﻿using UnityEngine;
using System.Collections;

public class BossDeath : MonoBehaviour {


    public HasLife hasLife;
    void Start() {

        hasLife.onNoLifeAnymore += Death;
    }

    private void Death(HasLife obj)
    {
        this.gameObject.SetActive(false);
        Destroy(this.gameObject);
    }

    void OnDestroy() {

        hasLife.onNoLifeAnymore -= Death;
    }

}
