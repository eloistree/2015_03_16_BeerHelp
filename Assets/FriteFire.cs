﻿using UnityEngine;
using System.Collections;

public class FriteFire : MonoBehaviour {

    public string poolNameToLoad = "Frites";
    public PoolGenerator frenchFritePool;
    public float range = 0.05f;

    public bool fireing;

    public float minBetweenFire = 0.2f;
    public float maxBetweenFire = 0.4f;
    public float next;

    void Start() {

        if( frenchFritePool==null)
        frenchFritePool = PoolGenerator.GetPool(poolNameToLoad);
    
    }

    public Vector3 direction;
	void Update () {
        Vector3 screenPos = Input.mousePosition;
        screenPos.z = 0;
        Vector3 gunScreenPos = Camera.main.WorldToScreenPoint(transform.position);
        direction = screenPos - gunScreenPos;
        direction.z = 0;

        transform.rotation = Quaternion.LookRotation(direction);

        if (Input.GetMouseButtonDown(0)) {
            Fire();
        }

        fireing = Input.GetMouseButton(0);
        if (fireing ) {
            if (next > 0) {
                next -= Time.deltaTime;
            }
            if (next <= 0) {
                next = Random.Range(minBetweenFire,maxBetweenFire);
                Fire();
            }

        }

	}

    private void Fire()
    {
        if (frenchFritePool == null) return;
        GameObject nextBullet = frenchFritePool.GetNextAvailable();
        if (nextBullet != null)
        {
            Vector3 from = transform.position;
            from.x += Random.Range(-range, range);
            from.y += Random.Range(-range, range);
            nextBullet.transform.position = from;
            nextBullet.transform.rotation = transform.rotation;
            nextBullet.SetActive(true);
        }   
    }
}
