﻿using UnityEngine;
using System.Collections;

public class OutOfCameraSetOff : MonoBehaviour {

    public Vector3 view;
    public float rangeMin=-0.5f;
    public float rangeMax=1.5f;
	void Update () {

        view = Camera.main.WorldToScreenPoint(this.transform.position);
        if (view.x < (rangeMin * Screen.width) || view.x > (rangeMax * Screen.width) || view.y > (rangeMax * Screen.height) || view.y < (rangeMin * Screen.height))
            this.gameObject.SetActive(false);

	}
}
