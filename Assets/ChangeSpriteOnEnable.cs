﻿using UnityEngine;
using System.Collections;

public class ChangeSpriteOnEnable : MonoBehaviour {

    public SpriteRenderer renderer;
    public Sprite[] spriteRef ; 

    void OnEnable() {

        renderer.sprite = spriteRef[Random.Range(0, spriteRef.Length)];
    
    }
}
